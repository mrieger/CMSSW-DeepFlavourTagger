# -*- coding: utf-8 -*-

"""
This script creates a dummy graph that resembles the structure of the final deep flavour NN.
It is batched and takes 10 (random) input features and outputs 5 (also random) values representing
the probabilities for the different jet flavours: b, bb, c, cc, l.
"""

# imports
import os
import sys

# paths
thisdir = os.path.dirname(os.path.abspath(__file__))
dummydir = os.path.join(thisdir, "dummygraph")

# make sure tensorflow is importable, then import it
sys.path.insert(0, os.path.expandvars("$CMSSW_BASE/src/DNN/Tensorflow/python"))
import tensorflow as tf

# network design
x_ = tf.placeholder(tf.float32, [None, 10], name="input")

W = tf.Variable(tf.ones([10, 5]))
b = tf.Variable(tf.ones([5]))
h = tf.matmul(x_, W) + b

y = tf.nn.softmax(h, name="output")

# session
sess = tf.Session()
sess.run(tf.global_variables_initializer())

# test, should print 0.2
print(sess.run(y, feed_dict={x_: [range(10)]})[0][0])

# save the graph
if not os.path.exists(dummydir):
    os.makedirs(dummydir)
saver = tf.train.Saver()
saver.save(sess, os.path.join(dummydir, "dummygraph_v1"))
