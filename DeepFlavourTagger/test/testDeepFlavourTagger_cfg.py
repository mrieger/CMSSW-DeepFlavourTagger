# -*- coding: utf-8 -*-

"""
DeepFlavourTagger test config.
"""


import FWCore.ParameterSet.Config as cms


#
# process definition and general setup
#

process = cms.Process("TEST")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
        # "file:///net/scratch_cms/institut_3a/rieger/public/ttH125_bb_80X.root"
        "root://cms-xrd-global.cern.ch//store/mc/RunIISummer16MiniAODv2/ttHTobb_M125_TuneCUETP8M2_ttHtranche3_13TeV-powheg-pythia8/MINIAODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/50000/0C0796F5-CFC6-E611-ADB6-008CFA0A58B4.root"
    )
)

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(1)
)

process.options = cms.untracked.PSet(
    allowUnscheduled = cms.untracked.bool(True),
    wantSummary      = cms.untracked.bool(False)
)


#
# DeepFlavourTagger
#

process.load("DeepFlavourTagger.DeepFlavourTagger.deepFlavourTagger_cfi")

# no dropout for the moment
process.deepFlavourTagger.dropoutName = cms.string("")



#
# process path
#

process.p = cms.Path(process.deepFlavourTagger)
