/*
 * DeepFlavourTagger
 *
 * Description:
 *     TODO.
 *
 * Author:
 *     Marcel Rieger
 *
 * Created:
 *     11 May 2017
 */

#include <memory>

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/Framework/interface/stream/EDProducer.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/StreamID.h"

#include "DataFormats/PatCandidates/interface/Jet.h"

#include "DNN/Tensorflow/interface/Tensor.h"
#include "DNN/Tensorflow/interface/Graph.h"

enum FeatureVersion
{
    FV_V0 = 0
};

class DeepFlavourTagger : public edm::stream::EDProducer<>
{
public:
    explicit DeepFlavourTagger(const edm::ParameterSet&);
    ~DeepFlavourTagger();

    static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
    virtual void produce(edm::Event&, const edm::EventSetup&) override;

    void fillJetFeatures(int i, pat::Jet& jet);

    // configs
    std::string graphPath_;
    std::string featureVersion_;
    edm::InputTag jetCollection_;

    // tokens
    edm::EDGetTokenT<std::vector<pat::Jet> > jetToken_;

    // other members
    FeatureVersion v_;
    dnn::tf::Tensor* inputTensor_;
    dnn::tf::Tensor* outputTensor_;
    dnn::tf::Tensor* dropoutTensor_;
    dnn::tf::Graph* graph_;
    int nFeatures_;
};

DeepFlavourTagger::DeepFlavourTagger(const edm::ParameterSet& iConfig)
    : graphPath_(iConfig.getParameter<std::string>("graphPath"))
    , featureVersion_(iConfig.getParameter<std::string>("featureVersion"))
    , jetCollection_(iConfig.getParameter<edm::InputTag>("jetCollection"))
    , inputTensor_(0)
    , outputTensor_(0)
    , dropoutTensor_(0)
    , graph_(0)
    , nFeatures_(0)
{
    // product registration
    // produces<std::vector<pat::Jet>>();

    // setup tokens
    jetToken_ = consumes<std::vector<pat::Jet> >(jetCollection_);

    // initialize the graph
    std::string fullGraphPath;
    if (graphPath_.substr(0, 1) == "/" || graphPath_.substr(0, 5) == "file:")
    {
        fullGraphPath = graphPath_;
    }
    else
    {
        std::string cmsswBase = std::string(getenv("CMSSW_BASE"));
        std::string dataDir = cmsswBase + "/src/DeepFlavourTagger/DeepFlavourTagger/data";
        fullGraphPath = dataDir + "/" + graphPath_;
    }
    graph_ = new dnn::tf::Graph(fullGraphPath);

    // parse and assign the correct feature version
    if (featureVersion_ == "v0")
    {
        v_ = FV_V0;
        nFeatures_ = 10;
    }
    else
    {
        throw std::runtime_error("unkown featureVersion: " + featureVersion_);
    }

    // define tensors
    std::string inputName = iConfig.getParameter<std::string>("inputName");
    std::string outputName = iConfig.getParameter<std::string>("outputName");
    std::string dropoutName = iConfig.getParameter<std::string>("dropoutName");
    // the shape of the input tensor is set for every event to adjust the batch size to n jets
    inputTensor_ = graph_->defineInput(new dnn::tf::Tensor(inputName));
    // the shape of the output tensor is set internally after each eval call
    outputTensor_ = graph_->defineOutput(new dnn::tf::Tensor(outputName));
    // optional dropout
    if (!dropoutName.empty())
    {
        dropoutTensor_ = graph_->defineInput(new dnn::tf::Tensor(dropoutName, 0, 0));
        dropoutTensor_->setValue<float>(1.);
    }
}

DeepFlavourTagger::~DeepFlavourTagger()
{
    if (graph_)
    {
        // delete the graph, which itself deletes all tensors
        delete graph_;
    }
}

void DeepFlavourTagger::produce(edm::Event& iEvent, const edm::EventSetup& iSetup)
{
    // get jets, make a copy of the vector to make them editable
    edm::Handle<std::vector<pat::Jet> > jetsHandle;
    iEvent.getByToken(jetToken_, jetsHandle);
    std::vector<pat::Jet> jets(*jetsHandle);

    // set the input tensor shape for batching
    dnn::tf::Shape inputShape[] = {(dnn::tf::Shape)jets.size(), (dnn::tf::Shape)nFeatures_};
    inputTensor_->setArray(2, inputShape);

    // set features per jet
    for (int i = 0; i < (int)jets.size(); i++)
    {
        fillJetFeatures(i, jets[i]);
    }

    // eval the batch of jets
    graph_->eval();

    // read the values
    for (int i = 0; i < (int)jets.size(); i++)
    {
        std::cout << "jet " << i << std::endl;
        std::cout << "prob b:  " << outputTensor_->getValue<float>(i, 0) << std::endl;
        std::cout << "prob bb: " << outputTensor_->getValue<float>(i, 1) << std::endl;
        std::cout << "prob c:  " << outputTensor_->getValue<float>(i, 2) << std::endl;
        std::cout << "prob cc: " << outputTensor_->getValue<float>(i, 3) << std::endl;
        std::cout << "prob l:  " << outputTensor_->getValue<float>(i, 4) << std::endl;
        std::cout << "------------------" << std::endl;
        // TODO: store the values within the jet
    }

    // store the update jet collection
    // std::unique_ptr<std::vector<pat::Jet> > jetsPtr(jets);
    // iEvent.put(std::move(jetsPtr));
}

void DeepFlavourTagger::fillJetFeatures(int i, pat::Jet& jet)
{
    if (v_ == FV_V0)
    {
        // dummy features
        for (int j = 0; j < nFeatures_; j++)
        {
            inputTensor_->setValue<float>(i, j, (float)j);
        }
    }
}

void DeepFlavourTagger::fillDescriptions(edm::ConfigurationDescriptions& descriptions)
{
    edm::ParameterSetDescription desc;

    desc.add<std::string>("graphPath", "dummygraph/dummygraph_v1");
    desc.add<std::string>("featureVersion", "v0");
    desc.add<std::string>("inputName", "input:0");
    desc.add<std::string>("outputName", "output:0");
    desc.add<std::string>("dropoutName", "dropout:0");
    desc.add<edm::InputTag>("jetCollection", edm::InputTag("slimmedJets"));

    descriptions.add("DeepFlavourTagger", desc);
}

DEFINE_FWK_MODULE(DeepFlavourTagger);
