# -*- coding: utf-8 -*-

"""
DeepFlavourTagger module initialization.
"""


import FWCore.ParameterSet.Config as cms


deepFlavourTagger = cms.EDProducer("DeepFlavourTagger",
    graphPath      = cms.string("dummygraph/dummygraph_v1"),
    featureVersion = cms.string("v0"),
    inputName      = cms.string("input:0"),
    outputName     = cms.string("output:0"),
    dropoutName    = cms.string("dropout:0"),
    jetCollection  = cms.InputTag("slimmedJets")
)
