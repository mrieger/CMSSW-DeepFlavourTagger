# Deep Flavour Tagger

### Testing

```bash
source /cvmfs/cms.cern.ch/cmsset_default.sh

export SCRAM_ARCH="slc6_amd64_gcc530"
export CMSSW_VERSION="CMSSW_8_0_26_patch2"

cmsrel $CMSSW_VERSION
cd $CMSSW_VERSION/src
cmsenv

git clone https://gitlab.cern.ch/mrieger/CMSSW-DNN.git DNN
./DNN/setup.sh

git clone https://gitlab.cern.ch/mrieger/CMSSW-DeepFlavourTagger.git DeepFlavourTagger

scram b -j

cmsRun DeepFlavourTagger/DeepFlavourTagger/test/testDeepFlavourTagger_cfg.py
```
